import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserdataService {
  constructor() {}
  users() {
    return [
      {
        name: 'tushar',
        email: 'tushar@gmail.com',
      },
      {
        name: 'sam',
        email: 'sam@gmail.com',
      },
      {
        name: 'vaibhav',
        email: 'vaibhva@gmail.com',
      },
    ];
  }
}
